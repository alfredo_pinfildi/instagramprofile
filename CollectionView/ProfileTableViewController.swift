//
//  ProfileTableViewController.swift
//  CollectionView
//
//  Created by alfredo pinfildi on 27/10/2018.
//  Copyright © 2018 alfredo pinfildi. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    let user = User.userInstance
    var pictures: [String] = []
    @IBOutlet weak var picturesCollection: UICollectionView!
    @IBOutlet weak var nameSurname: UILabel!
    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var profileImage: UIButton!
    @IBAction func profileImage(_ sender: Any) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = user.username
        nameSurname.text = user.name + " " + user.surname

        let profileImageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 96, height: 96))
        let image: UIImage = UIImage(named: "profile")!
        profileImageView.image = image
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.clipsToBounds = true
        profileImageView.contentMode = .scaleAspectFill
        profileImage.addSubview(profileImageView)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = user.username
        tableView.tableFooterView = UIView()
        editProfile.layer.borderWidth = 0.4
        editProfile.layer.cornerRadius = 5
        editProfile.layer.borderColor = UIColor.lightGray.cgColor
        profileImage.layer.cornerRadius = 48
        profileImage.layer.borderColor = UIColor.black.cgColor
        profileImage.backgroundColor = .clear
        initializePictures()
        
    }
    
    
    func initializePictures() {
        
        for index in 0...12 {
            
            pictures.append("\(index + 1)")
            
        }
   
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return pictures.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PictureCollectionViewCell", for: indexPath) as! PictureCollectionViewCell
        cell.picture.image = UIImage(named: "\(pictures[indexPath.row])")
    
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width - 2) / 3, height: (collectionView.bounds.width - 2) / 3)
        
    }
    
    /*override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        
        case 0:
            return CGFloat(154)
        case 2:
            return picturesCollection.collectionViewLayout.collectionViewContentSize.height
        default:
            return UITableView.automaticDimension
            
        }
    }*/
    
}
