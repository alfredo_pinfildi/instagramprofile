//
//  User.swift
//  CollectionView
//
//  Created by alfredo pinfildi on 27/10/2018.
//  Copyright © 2018 alfredo pinfildi. All rights reserved.
//

import Foundation

class User {
    
    static let userInstance = User()
    
    
    
    private init() {}
    
    
    var username: String {
        
        get {
            
            return UserDefaults.standard.string(forKey: "userUsername") ?? "username"
            
        }
        
        set (username) {
            
            UserDefaults.standard.set(username, forKey: "userUsername")
            
        }
        
    }
    
    
    var name: String {
    
        get {
            
            return UserDefaults.standard.string(forKey: "userName") ?? "Name"
            
        }
        
        set (name) {
            
            UserDefaults.standard.set(name, forKey: "userName")
        
        }
        
    }
    
    
    var surname: String {
        
        get {
            
            return UserDefaults.standard.string(forKey: "userSurname") ?? "Surname"
            
        }
        
        set (surname) {
            
            UserDefaults.standard.set(surname, forKey: "userSurname")
            
        }
        
    }
    
}
