//
//  PictureCollectionViewCell.swift
//  CollectionView
//
//  Created by alfredo pinfildi on 28/10/2018.
//  Copyright © 2018 alfredo pinfildi. All rights reserved.
//

import UIKit

class PictureCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var picture: UIImageView!
    
}
