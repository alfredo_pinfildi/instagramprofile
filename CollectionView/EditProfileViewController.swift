//
//  EditProfileViewController.swift
//  CollectionView
//
//  Created by alfredo pinfildi on 27/10/2018.
//  Copyright © 2018 alfredo pinfildi. All rights reserved.
//

import UIKit

class EditProfileViewController: UITableViewController {
    
    let user = User.userInstance
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var done: UIBarButtonItem!
    @IBOutlet weak var username: UITextField!
    @IBAction func done(_ sender: Any) {
        
        user.name = name.text!
        user.surname = surname.text!
        user.username = username.text!
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        name.text = user.name
        surname.text = user.surname
        username.text = user.username

    }

}
